# Actuation Conflict Manager (ACM)

IoT applications have been limited to collecting field information for a long time. 
However, Smart IoT Systems (SIS) are now not only composed of sensors, but also of actuators taking electrical input and turning it into physical actions. 
Therefore, in case of infrastructures with shared actuators, interactions with the physical environment raise additional challenges that cannot be ignored
Actuators not only raise concurrent and possibly conflicting accesses problems. 
They also raise problems of semantic coherency between the considered actions and their resulting effects in the environment (e.g., opening a window while heating).
The consequences of actions and their impacts in the physical environment may put at risk applications functionalities.
Indeed, applications being no longer isolated processes, they are not immune to the effects of the concurrent applications sharing the same environment and potentially producing antagonistic effects.

Actuation Conflict Manager (ACM) helps to identify, analyse and resolve different kind of conflicts (direct and indirect ones) so as to ensure SIS trustworthiness.

## Quick Install

To install ACM, clone this git repository. After downloading it, you need to configure it beacuse ACM need some external tools. Refer to the [Dependencies and Setting up sections](./src/README.md#dependencies)

## Table of contents
- [Running ACM](./src/README.md)
  - Setting up ACM
- [Model documentation](./docs/README.md)
  - WIMAC model schemas
- [Usage tutorials](./docs/tutorials/README.md)
  - ACM step by step usage tutorials
- [Demos](./demos/README.md)
  - ACM demonstration codes