# ACM demo index

These demos are sorted by order of implementation (the first ones are the most recent).

- ["HomeIO" Use-case demonstration](./homeio)
  - Sources of the demonstration with the little cute wooden house
- ["House Doll" demonstration](./demo-pizero)
  - Sources of the demonstration with the little cute wooden house
- [Android demo](./demo-android)
  - Sources to deploy node-red on android
- [D2.2 demo](./demo-d2.2)
  - Sources for D2.2 demo

You can also find in this directory stuff for Hackathon organized in Nice - Université Côte d'Azur on March 2020.

- [Hackathon](./hachathon)
