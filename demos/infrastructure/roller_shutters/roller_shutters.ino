String str; // for incoming serial data
short pin;

short key_1 = 2;     // Relay for Key "1" on remote control connected to D2 (Key 1 = Kitchen roller shutter)
short key_2 = 3;     // Relay for Key "2" on remote control connected to D3 (Key 2 = Living Room bigest roller shutter)
short key_3 = 4;     // Relay for Key "3" on remote control connected to D4 (Key 3 = Living Room smaller roller shutter)
short key_6 = 5;     // Relay for Key "6" on remote control connected to D5 (Key 6 = all)
short key_Up = 6;    // Relay for Key "Up" on remote control connected to D6
short key_Stop = 7;  // Relay for Key "Stop" on remote control connected to D7
short key_Down = 8;  // Relay for Key "Down" on remote control connected to D8

void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  pinMode(key_1, OUTPUT);
  pinMode(key_2, OUTPUT);
  pinMode(key_3, OUTPUT);
  pinMode(key_6, OUTPUT);
  pinMode(key_Up, OUTPUT);
  pinMode(key_Stop, OUTPUT);
  pinMode(key_Down, OUTPUT);
}

void loop() {
  // send data only when you receive data:
  if (Serial.available() > 0) {
    // read the incoming byte:
    str = Serial.readStringUntil('\n');

    switch (str[0]) {
      case '1': pin = key_1;    break;
      case '2': pin = key_2;    break;
      case '3': pin = key_3;    break;
      case '6': pin = key_6;    break;
      case 'U': pin = key_Up;   break;
      case 'S': pin = key_Stop; break;
      case 'D': pin = key_Down; break;
      default:
        pin = -1;
        Serial.println("Error: key function not specified correctly (should be 1, 2, 3, 6, U, S or D)");
    }

    if (pin != -1) {
      Serial.print("D"); Serial.print(pin); Serial.print("=1"); 
      digitalWrite(pin, HIGH);
      delay(500);
      Serial.println("=0");
      digitalWrite(pin, LOW);
      delay(250);
    }
    
  }
}
