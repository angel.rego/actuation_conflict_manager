package ENACTProducer.logic;

import java.io.IOException;
import java.util.Observer;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.smool.kpi.model.exception.KPIModelException;

import ENACTProducer.api.Consumer;
import ENACTProducer.api.MessageReceiveSensorSubscription;
import ENACTProducer.api.Producer;
import ENACTProducer.api.SmoolKP;
import ENACTProducer.model.smoolcore.IMessage;
import ENACTProducer.model.smoolcore.impl.Message;
import ENACTProducer.model.smoolcore.impl.MessageReceiveSensor;

import ENACTProducer.logic.ACM_GeneSIS_Demo_Common;

public class MqttClientEnact implements MqttCallback{

	public IMqttClient publisher; 
	CustomActuation sensor_luminance_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.sensor_luminance_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation camera_detection_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.camera_detection_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation botvac_state_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.botvacD3_state_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation botvacD3_action_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.botvacD3_action_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation botvacD3_is_docked_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.botvacD3_is_docked_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation botvacD3_is_scheduled_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation botvacD3_is_charging_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.botvacD3_is_charging_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation microphone_sound_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.microphone_sound_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation microphone_zcr_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.microphone_zcr_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation microphone_mfcc_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.microphone_mfcc_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation microphone_time_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.microphone_time_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation sensors_cec_status_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.sensors_cec_status_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);
	CustomActuation sensors_cec_source_ca = new CustomActuation(ACM_GeneSIS_Demo_Common.sensors_cec_source_id, ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor);


    public MqttClientEnact() throws Exception{
        initMQTTclient();
    }

    private void initMQTTclient() throws InterruptedException {
		try{
        	// ---------------------------MQTT Client----------------------------------
    		String publisherId = UUID.randomUUID().toString();
    		//Connect to the MQTT broker that is connected to HomeIO app
    		//publisher = new MqttClient("tcp://192.168.1.28:1883", publisherId);

    		publisher = new MqttClient("tcp://192.168.0.27:1883", publisherId);
//    		publisher = new MqttClient("tcp://10.0.0.13:1883", publisherId);
            
            MqttConnectOptions options = new MqttConnectOptions();
            options.setAutomaticReconnect(true);
            options.setCleanSession(true);
            options.setConnectionTimeout(10);
			publisher.setCallback(this);
            publisher.connect(options);
			System.out.println("Connected to the broker to SMART BUILDING");
			
//			MqttTopic topic = publisher.getTopic(sensor_luminance_topic);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.sensor_luminance_topic, 0);
//			topic = publisher.getTopic(camera_detection_topic);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.camera_detection_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.botvacD3_state_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.botvacD3_action_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.botvacD3_is_docked_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.botvacD3_is_charging_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.microphone_sound_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.microphone_zcr_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.microphone_mfcc_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.microphone_time_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.sensors_cec_status_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.sensors_cec_source_topic, 0);
			
		}catch(MqttException e){
        	e.printStackTrace();
			Thread.sleep(5000);
//			System.out.println("WAITING for the CONNECTION to the broker of the SMART BUILDING...");
//			initMQTTclient();
            //throw new RuntimeException("Exception occurred in creating MQTT Client");
        }catch(Exception e) {
        	//Unable to connect to server (32103) - java.net.ConnectException: Connection refused
        	e.printStackTrace();
			System.exit(1);
        }
	}

    /**
	FROM APP to SMOOL
	*/
	 @Override
    public void connectionLost(Throwable t) {
        System.out.println("Connection to SMART BUILDING lost!");
        
        // code to reconnect to the broker would go here if desired
        try {
        	System.out.println("RECONNECTING to the broker of the SMART BUILDING...");
			initMQTTclient();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
		//Sending sensor data from SMART BUILDING to SMOOL
        	
		String message_payload = new String(mqttMessage.getPayload());
		System.out.println("Forwarding to SMOOL: " + s + " = " + message_payload);
		
		String timestamp = Long.toString(System.currentTimeMillis());
		
		Message msg = new Message();
		msg.setBody(message_payload);
		//msg.setTimestamp(timestamp);
			
			if(s.equals(ACM_GeneSIS_Demo_Common.sensor_luminance_topic)){
				new Thread(() -> sensor_luminance_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_luminance._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.camera_detection_topic)){
				new Thread(() -> camera_detection_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.camera_detection._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.botvacD3_state_topic)){
				new Thread(() -> botvac_state_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_state._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.botvacD3_action_topic)){
				new Thread(() -> botvacD3_action_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_action._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.botvacD3_is_docked_topic)){
				new Thread(() -> botvacD3_is_docked_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_is_docked._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled_topic)){
				new Thread(() -> botvacD3_is_scheduled_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_is_scheduled._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.botvacD3_is_charging_topic)){
				new Thread(() -> botvacD3_is_charging_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.botvacD3_is_charging._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.microphone_sound_topic)){
				new Thread(() -> microphone_sound_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_sound._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.microphone_zcr_topic)){
				new Thread(() -> microphone_zcr_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_zcr._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.microphone_mfcc_topic)){
				new Thread(() -> microphone_mfcc_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_mfcc._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.microphone_time_topic)){
				new Thread(() -> microphone_time_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.microphone_time._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.sensors_cec_status_topic)){
				new Thread(() -> sensors_cec_status_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensors_cec_status._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
			
			if(s.equals(ACM_GeneSIS_Demo_Common.sensors_cec_source_topic)){
				new Thread(() -> sensors_cec_source_ca.run(msg)).start();
				//SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensors_cec_source._getIndividualID(), ACM_GeneSIS_Demo_Common.name, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
			}
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
//        try {
//			System.out.println("Forwarding to SMOOL completed" + new String(iMqttDeliveryToken.getMessage().getPayload()));
//		} catch (MqttException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    }

}