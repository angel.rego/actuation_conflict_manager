#include "DHT.h"

#define DHTPIN  2
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

#include"AirQuality.h"
#include"Arduino.h"
AirQuality airqualitysensor;
int current_quality =-1;

#include <math.h>
#define Vc 4.95
//the number of R0 you detected just now
#define R0 34.28

void setup() {
  Serial.begin(9600);
  airqualitysensor.init(A0);
  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);
  
  //light
  int lightValue = analogRead(A2);
  Serial.print("Brightness = ");
  Serial.println(lightValue);
  //END light
  
  //HCHO
    int sensorValue=analogRead(A6);
    double Rs=(1023.0/sensorValue)-1;
    Serial.print("HCHO Rs = ");
    Serial.println(Rs);    
    Serial.print("HCHO R0 = ");
    Serial.println(R0);
    double ppm=pow(10.0,((log10(Rs/R0)-0.0827)/(-0.4807)));
    Serial.print("HCHO ppm = ");
    Serial.println(ppm);
  //END HCHO


  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  // Compute heat index in Celsius (isFahreheit = false)
  //float hic = dht.computeHeatIndex(t, h, false);

  Serial.print("Humidity = ");
  Serial.println(h);
  Serial.print("Temperature = ");
  Serial.println(t);

  //AIR QUALITY

      current_quality=airqualitysensor.slope();
    if (current_quality >= 0)// if a valid data returned.
    {
        Serial.print("Air Quality = ");
        if (current_quality==0)
            Serial.println(4);
        else if (current_quality==1)
            Serial.println(3);
        else if (current_quality==2)
            Serial.println(2);
        else if (current_quality ==3)
            Serial.println(1);
    }
}
ISR(TIMER2_OVF_vect)
{
    if(airqualitysensor.counter==122)//set 2 seconds as a detected duty
    {
        airqualitysensor.last_vol=airqualitysensor.first_vol;
        airqualitysensor.first_vol=analogRead(A3);
        airqualitysensor.counter=0;
        airqualitysensor.timer_index=1;
        PORTB=PORTB^0x20;
    }
    else
    {
        airqualitysensor.counter++;
    }
}
  // END AIR QUALITY
