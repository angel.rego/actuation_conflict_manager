# Home I/O demo

## Intall Home I/O software

Home I/O is a "smart" house simulator. We will use Home I/O to simulate a home equipped with many connected sensors and actuators. Through this software, it will be possible to have access to simulated data (temperature, user’s presence, ...) but also to act on the environment (turn on a light, control the heating, close the blinds, ...).

![Home I/O](./_media/homeio.jpg)

You can download it on the following website:
[https://teachathomeio.com/](https://teachathomeio.com/)

This software will be available for free during 1 month after installation.

To familiarize yourself with the simulation environment, we invite you to consult the documentation:
[https://realgames.co/docs/homeio/en/](https://realgames.co/docs/homeio/en/)

We especially invite you to consult:

- [The sensors and actuators map](https://realgames.co/docs/homeio/en/devices-map/)
- [The device control mode](https://realgames.co/docs/homeio/en/device-modes/)

## Configure simulation

You must first configure the Home I/O in English (to normalize sensors and actuators naming). To achieve this:

- Go to Home I/O Localisation Menu
- Select Language: English
- Select Units: Metric

Regarding the control mode of the devices, they must be set in the "External" mode to be controlled by a third-party application (and not by clicking in the simulator on all devices). You can configure Home I/O sensors and actuators in two differents ways:

- Create you own simulation configuration
- Reuse an already configured simulation

### Configure your own Home I/O simulation

You must allow all devices to be controlled by third-party application. To achieve this, you can click on all the small symbols to change them to blue (external mode)... but it may take you a half dayand you may forget one.

![Red = wired / Blue = external](./_media/wired_external.jpg)

The easiest way is to save the simulation:

- Create a new simulation in Home I/O
- Save it (you don't need to manually configure each device)
- Go to folder "Documents/Home IO/Saves"
- Edit the xml file containing the sensor and actuator configurationwith your favourite text editor
- Replace the word "Wired" with the word "External".
- Save the document
- Load the simulation in Home I/O and check if devices turned “blue”.

### Reuse a provided simulation configuration

You can also reuse a file provided in the directory homeio-config, like:

- [Kubik Tecnalia_2020_4_30_15_3_7.xml](./homeio-config/Kubik Tecnalia_2020_4_30_15_3_7.xml)

Just copy this file in your "Document/Home IO/Saves" folder and load it in Home I/O software. It's a ready to use simulation.

## Home I/O MQTT bridge

In folder homeio-mqtt you will find a software to lauch to provide a Home I/O to MQTT connection. This will allow to send data or receive commands from and to Home I/O through a MQTT broker.

Launch ````HomeIO_MQTT.exe```` The software will try to atomactically connect to a locahost MQTT broker.

You can also specify the IP address of a MQTT broker if the broker is not on the same machine as HomeIO and HomeIO_MAQTT are running. Use ````HOMEIO_MQTT.exe 192.168.0.54```` to publish to a broker running on 192.168.0.54 for instance.

## ENACT files

We provide:

- a GeneSIS deployment model (deployment_model.json)
- the node-red flows deployed by GeneSiS (homeio-flows.json)
- an ACM environment model (acm_env_model.json)

## Using node red flows
 - If you use the same device configuration you only have to change the MQTT BROKER adress to match yours. (the broker should be where home io is executed)
 - If you want to add a new device, you need first to add the MQTT IN or OUT node in either Sensors HOME IO or Actuators HOME IO flows. Then you create a link in the interface flows and link it.
 
## Sensors
 - Doors : Boolean (false for open & true for closed) / the msg.topic should be different for each doors (now only 2 doors are supported)
 - Temperature : Float or a Number
 - Motion Detector : Boolean
 - Brightness : Float or Number (value for openhab is not yet adjusted) (0 to 10)
 - Power : Number (0 to 2500 for now)
 - Date/Time : String
 - Cloudiness : Float (0.0 to 1.0 = 0% to 100%)
 - Smoke : Boolean
 - Shades : Float (0 to 10)

## Actuators
 - Siren : Boolean
 - Roller Shades Up / Down : Boolean
 - Lights : Number (0 to 10)
 - Heater : Number (0 to 10)