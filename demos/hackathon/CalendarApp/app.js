const express = require("express");
const fs = require("fs");
const jwt = require('jsonwebtoken');
const jwkToPem = require('jwk-to-pem');

let app = express();
app.use(express.static('public'));
app.use(express.json({ limit: '10mb' }));

let CalendarList = [{ "id": "1", "name": "Meeting room", "checked": true, "color": "#ffffff", "bgColor": "#9e5fff", "borderColor": "#9e5fff", "dragBgColor": "#9e5fff" }];
let ScheduleList = [];

let caacenable = false;

if (fs.existsSync("CalendarList.json")) {
    CalendarList = JSON.parse(fs.readFileSync("CalendarList.json"));
}
if (fs.existsSync("ScheduleList.json")) {
    ScheduleList = JSON.parse(fs.readFileSync("ScheduleList.json"));
}
if (fs.existsSync("settings.json")) {
    let settings = JSON.parse(fs.readFileSync("settings.json"));
    caacenable = settings.caacenable;
}

let evidianjwk = {
    "keys": [{
        "kty": "RSA",
        "e": "AQAB",
        "use": "sig",
        "kid": "rsa256",
        "alg": "RS256",
        "n": "-zqG44QKlxkW9YVUZ1Y6qtzIGPt3sk0Lg1RbaqKiXjpwX0TrCNbRMTxCGWEOPyz-gNrxAeKP3GeoN2qvhwwHxXrHvHfTAYt6jJoUQcmzZlKyAc6gNPPxVRyL0NJKikI4BVxjAy4u-4gXFeMtV6y1PZSJqAN4UzSP3X3zeA0c4Mpw0BImc1JOWXj_tEQdUfDZP8ht2l4tnqXnSYKQjpDVIdPzJfbzVOTPv_36uFKon6XOxskCytW-rZbFDYTD6kKllqAHV6i1CCZzRwGaY5nq9Q_zIof8ezW-WUdzvKMvRu9x0tt1BZjkEmFoiMhTtyjn0-c-r866FG8SgYIfWLv6iw"
    }]
};
let evidianpem = jwkToPem(evidianjwk.keys[0]);


function AuthenticateRequest(req) {
    // special browser bypass hack to avoid having to get a token for it
    if (req.get("X-CAAC-isbrowser")) return true;
    try {
        jwt.verify(
            req.get("Authorization").split(" ")[1],
            evidianpem,
            { algorithms: ['RS256'] }
        );
        console.log("Token OK");
        return true;
    } catch (e) {
        console.error("Error authenticating CAAC request");
        console.error(e);
        return false;
    }
}

app.get("/calendars", (req, res) => {
    if (caacenable && !AuthenticateRequest(req)) {
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify({ status: "auth error" }));
        return;
    }

    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(CalendarList));
});

app.get("/schedules", (req, res) => {
    if (caacenable && !AuthenticateRequest(req)) {
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify({ status: "auth error" }));
        return;
    }

    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(ScheduleList));
});

app.get("/schedules/now", (req, res) => {
    if (caacenable && !AuthenticateRequest(req)) {
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify({ status: "auth error" }));
        return;
    }

    res.setHeader("Content-Type", "application/json");
    let now = new Date();
    res.end(
        JSON.stringify(
            ScheduleList.filter(
                sch => new Date(sch.start._date) < now && new Date(sch.end._date) > now
            )
        )
    );
});

app.post("/schedules", (req, res) => {
    if (caacenable && !AuthenticateRequest(req)) {
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify({ status: "auth error" }));
        return;
    }

    // lol
    // the event is fired a lot so just prevent duplicates
    // this is terrible yes i know
    ScheduleList = ScheduleList.filter(sch => sch.id !== req.body.id);
    ScheduleList.push(req.body);
    console.log("new schedule after add:" + ScheduleList.length);
    fs.writeFileSync("ScheduleList.json", JSON.stringify(ScheduleList));

    res.setHeader("Content-Type", "application/json");
    res.end('{"status":"ok"}');
});

app.delete("/schedules", (req, res) => {
    if (caacenable && !AuthenticateRequest(req)) {
        res.setHeader("Content-Type", "application/json");
        res.end(JSON.stringify({ status: "auth error" }));
        return;
    }

    ScheduleList = ScheduleList.filter(sch => sch.id !== req.body.id);
    console.log("new schedule after delete:" + ScheduleList.length);

    res.setHeader("Content-Type", "application/json");
    res.end('{"status":"ok"}');
});

app.post("/caac-enable", (req, res) => {
    caacenable = req.body.caac;
    console.log("CAAC Status: " + caacenable);

    fs.writeFileSync("settings.json", JSON.stringify({ caacenable: caacenable }));

    res.setHeader("Content-Type", "application/json");
    res.end('{"status":"ok"}');
});

app.get("/settings", (req, res) => {
    res.setHeader("Content-Type", "application/json");
    res.end('{"CAAC":"'+caacenable+'"}');
});

app.listen(80);

process.on('SIGINT', function () {
    console.log("Exiting");

    fs.writeFileSync("CalendarList.json", JSON.stringify(CalendarList));
    fs.writeFileSync("ScheduleList.json", JSON.stringify(ScheduleList));

    process.exit();
});