
// request calendar list from server
let xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        console.log(xhttp.responseText);

        // update calendar list from servor
        CalendarList = JSON.parse(xhttp.responseText);
        cal.setCalendars(CalendarList);
        refreshCalendarList();
    }
};
xhttp.open("GET", "/calendars", true);
xhttp.setRequestHeader("X-CAAC-isbrowser", true);
xhttp.send();

// request schedule list from server
let xhttp2 = new XMLHttpRequest();
xhttp2.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        console.log(xhttp2.responseText);

        // update event list from servor
        ScheduleList = JSON.parse(xhttp2.responseText);
        ScheduleList.forEach(sch => sch.start = new Date(sch.start._date));
        ScheduleList.forEach(sch => sch.end = new Date(sch.end._date));
        cal.createSchedules(ScheduleList);
        //cal.setSchedules(ScheduleList);

    }
};
xhttp2.open("GET", "/schedules", true);
xhttp2.setRequestHeader("X-CAAC-isbrowser", true);
xhttp2.send();

// send new events to server
cal.on('afterRenderSchedule', function (e) {
    let req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(req.responseText);
        }
    };
    req.open("POST", "/schedules", true);
    req.setRequestHeader("Content-Type", "application/json");
    req.setRequestHeader("X-CAAC-isbrowser", true);
    req.send(JSON.stringify(e.schedule));
});


// send deleted events to server
cal.on("beforeDeleteSchedule", function (e) {
    let req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(req.responseText);
        }
    };
    req.open("DELETE", "/schedules", true);
    req.setRequestHeader("Content-Type", "application/json");
    req.send(JSON.stringify(e.schedule));
});

let CAAC = false;
function CAACToggle() {
    CAAC = !CAAC;
    document.getElementById("caacbutton").innerText = "Turn " + (CAAC ? "off" : "on") + " Context-Aware Access Control";
    document.getElementById("caacstatus").style = "width:20px; height:10px; display: inline-block; background-color:" + (CAAC ? "green" : "red") + ";";

    let req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            console.log(req.responseText);
        }
    };
    req.open("POST", "/caac-enable", true);
    req.setRequestHeader("Content-Type", "application/json");
    req.send(JSON.stringify({"caac":CAAC}));

}

// request caac status from server
let xhttp3 = new XMLHttpRequest();
xhttp3.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        console.log(xhttp3.responseText);

        // update event list from servor
        let settings = JSON.parse(xhttp3.responseText);
        CAAC = settings.CAAC === "true";
        document.getElementById("caacbutton").innerText = "Turn " + (CAAC ? "off" : "on") + " Context-Aware Access Control";
        console.log("width:20px; height:10px; display: inline-block; background-color:" + (CAAC ? "green" : "red") + ";"); 
        document.getElementById("caacstatus").style = "width:20px; height:10px; display: inline-block; background-color:" + (CAAC ? "green" : "red") + ";";
        
        //cal.setSchedules(ScheduleList);

    }
};
xhttp3.open("GET", "/settings", true);
xhttp3.send();
