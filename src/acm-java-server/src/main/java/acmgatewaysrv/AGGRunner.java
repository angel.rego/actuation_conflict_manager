package acmgatewaysrv;

import agg.cons.AtomConstraint;
import agg.util.Pair;
import agg.xt_basis.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

//import javax.json.JsonObject;

public class AGGRunner {


    //	XtBasisUsing e =new XtBasisUsing();
    private final BaseFactory bf = BaseFactory.theFactory();

    private static GraGra graphGrammar;
    private static GraGra ruleGrammar;
    private final MorphCompletionStrategy strategy = CompletionStrategySelector.getDefault();

    private static Boolean DEBUG = false;

    private int NumberNewComponent = 0;
    private String AGGAddress = "/home/tibo/Documents/CNRS/PFE_2019/ACM/pfe/youssef/PFE/jsonTesting.ggx";
    //    private String AGGAddress = "/home/tibo/Documents/CNRS/PFE_2019/ACM/pfe/youssef/PFE/testingParallelmultipleUse.ggx";
    private JSONObject request;
    private ArrayList<JSONObject> custom_rules = new ArrayList<>();

    public AGGRunner(String filename, JSONObject runnerStrategy, boolean debugOverride) throws Exception {
        DEBUG = debugOverride;
        AGGAddress = filename;
        BaseFactory bf = BaseFactory.theFactory();
        graphGrammar = bf.createGraGra();
        //duplicate used file
        graphGrammar.load(AGGAddress);
        AGGAddress = AGGAddress.replace(".ggx", "") + "-output.ggx";
        graphGrammar.save(AGGAddress);
        getANameForEachNode(graphGrammar);
        graphGrammar.save(AGGAddress);

        //createRequestTest();
        //createRequestForACM();

        JSONArray rulesArray = runnerStrategy.getJSONArray("custom_rules");
        for (int i = 0; i < rulesArray.length(); i++) {
            custom_rules.add(rulesArray.getJSONObject(i));
        }
        request = runnerStrategy.getJSONObject("request");

        GraGra result = applyRequest(graphGrammar, request);
        result.save(AGGAddress);
    }

    private void getANameForEachNode(GraGra g) {
        List<Node> nodes = g.getGraph().getNodesList();
        for (int i = 0; i < nodes.size(); i++) {
            if (nodes.get(i).getObjectName() == "") {
                nodes.get(i).setObjectName("generated_name_" + i);
            }
        }
    }

    private void createRequestTest() throws JSONException {
        //this is creating the final request that must work no matter what
        request = new JSONObject();

        JSONObject newRule1 = new JSONObject();
        JSONObject JsubRule1 = new JSONObject();
        JSONObject j1 = new JSONObject();
        j1.put("Rule", 1);
        JSONObject j2 = new JSONObject();
        j2.put("Rule1", 20);
        JsubRule1.accumulate("sequence", j1);
        JsubRule1.accumulate("sequence", j2);
        newRule1.put("custom_newRule1", JsubRule1);
        System.out.println(newRule1);


        JSONObject newRule2 = new JSONObject();
        JSONObject JsubRule2 = new JSONObject();
        JSONObject j3 = new JSONObject();
        j3.put("Rule2", 1);
        JSONObject j4 = new JSONObject();
        j4.put("Rule3", 5);
        JsubRule2.accumulate("sequence", j3);
        JsubRule2.accumulate("sequence", j4);
        newRule2.put("custom_newRule2", JsubRule2);
        System.out.println(newRule2);

        JSONObject Jsequence1 = new JSONObject();
        Jsequence1.put("custom_newRule1", 20);
        JSONObject Jsequence2 = new JSONObject();
        Jsequence2.put("custom_newRule2", 2);

        custom_rules.add(newRule1);
        custom_rules.add(newRule2);

        request.accumulate("parallel", Jsequence1);
        request.accumulate("parallel", Jsequence2);
        System.out.println(request.toString());
    }

    private void createRequestForACM() throws JSONException {
        //this is creating the final request that must work no matter what
        request = new JSONObject();

        //ACM for SoftComp
        JSONObject newRule1 = new JSONObject();
        JSONObject JsubRule1 = new JSONObject();
        JSONObject j1 = new JSONObject();
        j1.put("AddACM", 1);
        JSONObject j2 = new JSONObject();
        j2.put("AddACM2", 20);
        JsubRule1.accumulate("sequence", j1);
        JsubRule1.accumulate("sequence", j2);
        newRule1.put("custom_addACMForSoft", JsubRule1);
        System.out.println(newRule1);

        //Add ACM for Action
        JSONObject newRule2 = new JSONObject();
        JSONObject JsubRule2 = new JSONObject();
        JSONObject j3 = new JSONObject();
        j3.put("AddACMToAction", 1);
        JSONObject j4 = new JSONObject();
        j4.put("AddACMToAction2", 20);
        JsubRule2.accumulate("sequence", j3);
        JsubRule2.accumulate("sequence", j4);
        newRule2.put("custom_addACMForAction", JsubRule2);
        System.out.println(newRule2);

        //Add ACM for Action
        JSONObject newRule3 = new JSONObject();
        JSONObject JsubRule3 = new JSONObject();
        JSONObject j5 = new JSONObject();
        j5.put("physicalConflict", 1);
        JSONObject j6 = new JSONObject();
        j6.put("physicalConflict2", 20);
        JsubRule3.accumulate("sequence", j5);
        JsubRule3.accumulate("sequence", j6);
        newRule3.put("custom_physicalConflict", JsubRule3);
        System.out.println(newRule3);

        custom_rules.add(newRule1);
        custom_rules.add(newRule2);
        custom_rules.add(newRule3);

        JSONObject Jsequence1 = new JSONObject();
        Jsequence1.put("custom_addACMForSoft", 20);
        JSONObject Jsequence2 = new JSONObject();
        Jsequence2.put("custom_addACMForAction", 20);
        JSONObject Jsequence3 = new JSONObject();
        Jsequence3.put("custom_physicalConflict", 20);

//        request.accumulate("parallel",Jsequence1);
        request.accumulate("parallel", Jsequence2);
        request.accumulate("parallel", Jsequence3);

        System.out.println("Request::");
        System.out.println(request.toString());
        System.out.println("--Rq");
    }

    private GraGra applyRequest(GraGra gragra, JSONObject request) throws Exception {
        if (request.has("parallel")) {
            if(DEBUG) System.out.println("parallel");
            ArrayList<GraGra> results = new ArrayList<>();
            JSONArray array = request.getJSONArray("parallel");
            GraGra newGraph = gragra;
            newGraph.save(AGGAddress);
            for (int i = 0; i < array.length(); i++) {
                results.add(applyRequest(newGraph, array.getJSONObject(i)));
                GraGra currentGraGra = bf.createGraGra();
                currentGraGra.load(AGGAddress);
                newGraph = currentGraGra;
            }
            //merge
            GraGra mergedGragra = merge(results);
            return mergedGragra;
        } else if (request.has("sequence")) {
            if(DEBUG) System.out.println("sequence");
            JSONArray array = request.getJSONArray("sequence");
            GraGra sequencedGragra = gragra;
            for (int i = 0; i < array.length(); i++) {
                sequencedGragra = applyRequest(sequencedGragra, array.getJSONObject(i));
            }
            return sequencedGragra;
        } else {
            JSONObject currentRule = null;
            for (JSONObject rule : custom_rules) {
                if (rule.names().get(0).equals(request.names().get(0))) {
                    currentRule = (JSONObject) rule.get((String) rule.names().get(0));
                }
            }
            if (currentRule != null) {
                //if custom rule
                GraGra newGragra = gragra;
                for (int i = 0; i < request.getInt((String) request.names().get(0)); i++) {
                    if(DEBUG) System.out.println("custom rule : " + currentRule);
                    newGragra = applyRequest(newGragra, currentRule);
                }
                return newGragra;
            } else {
                //else it means it's a classic one
                if(DEBUG) System.out.println("classic rule : " + request);
                return applyRule(gragra, request.names().getString(0), request.getInt(request.names().getString(0)));
            }
        }
    }

    public GraGra applyRule(GraGra gragra, String ruleName, int nbOfOccurence) throws Exception {
        int rule_i = 0;
        for (int i = 0; i < gragra.getEnabledRules().size(); i++) {
//            System.out.println("RULE USED : " + gragra.getEnabledRules().get(i).getName());
            if (gragra.getEnabledRules().get(i).getName().equals(ruleName)) {
                rule_i = i;
            }
        }
//        System.out.println("RULE USED : currently using rule number "+ rule_i);
        Pair<Object, String> pair = gragra.isReadyToTransform(true);
        Object test = null;
        if (pair != null)
            test = pair.first;
        if (test != null) {
            if (test instanceof Graph) {
                System.out.println("Grammar  " + gragra.getName()
                        + "  graph: " + gragra.getGraph().getName()
                        + "  is not ready for transform");
            } else if (test instanceof AtomConstraint) {
                String s0 = "Atomic graph constraint  \""
                        + ((AtomConstraint) test).getAtomicName()
                        + "\" is not valid. "
                        + "\nPlease check: "
                        + "\n  - graph morphism ( injective and total )  "
                        + "\n  - attribute context ( variable and condition declarations ).";
                System.out.println(s0);
            } else if (test instanceof Rule) {
                String s0 = "Rule  \""
                        + ((Rule) test).getName()
                        + "\" : "
                        + ((Rule) test).getErrorMsg()
                        + "\nPlease check: \n  - attribute settings of the new objects of the RHS \n  - attribute context ( variable and condition declarations ) of this rule.\nThe grammar is not ready to transform.";
                System.out.println(s0);
            }
            System.out.println("Grammar  " + gragra.getName()
                    + "  CANNOT TRANSFORM!");
            return null;
        }
        if (DEBUG) {
            System.out.println("Grammar  " + gragra.getName() + "  is ready to transform");
            System.out.println("Matching and graph transformation ");
        }


        if (DEBUG) {
            System.out.println(this.strategy);
            this.strategy.showProperties();
        }

        // Set graph transformation options
        Vector<String> gratraOptions = new Vector<String>();
        gratraOptions.add("CSP");
        gratraOptions.add("injective");
        gratraOptions.add("dangling");
        gratraOptions.add("NACs");
        gratraOptions.add("PACs");
        gratraOptions.add("GACs");
        gratraOptions.add("consistency");
        graphGrammar.setGraTraOptions(gratraOptions);


        if (DEBUG) {
            System.out.println("Grammar " + gragra.getName() + " saved in " + AGGAddress);
            System.out.println("Continue ...");
        }

        Match match = null;
/********************************************************************************************/
        // an example to apply a rule
        List<Rule> rules = gragra.getListOfRules();

        // an example to apply rule2
        Rule rule2 = rules.get(rule_i);

        if (DEBUG) {
            System.out.println("Apply  rule2  " + rule2.getName() + " so long as possible");
            System.out.println("Rule2  " + rule2.getName() + "    >> create match");
        }

        match = gragra.createMatch(rule2);
        match.setCompletionStrategy(this.strategy, true);
        int cpt = 0;
        try {
            while (match.nextCompletion() && (cpt < nbOfOccurence)) {
//                    done[i] = false;
                cpt++;
                System.out.println("number of time rule is applied : " + cpt);
                //showGraph(graphGrammar);
                if (DEBUG) {
                    System.out.println("Rule : match is complete");
                }
                if (match.isValid()) {
                    if (DEBUG) {
                        System.out.println("Rule :  match is valid");
                    }
//					Step step = new Step();
                    try {
                        StaticStep.execute(match);
                        //graphGrammar.save(url);
                        //graphGrammar.load(url);
                        rules = gragra.getListOfRules();
                        rule2 = rules.get(rule_i);
                        match = gragra.createMatch(rule2);
                        match.setCompletionStrategy(this.strategy, true);


                        if (DEBUG) {
                            System.out.println("Rule  " + match.getRule().getName() + " : step is done");
                        }
                    } catch (TypeException ex) {
                        ex.printStackTrace();
                        gragra.destroyMatch(match);
                        if (DEBUG) {
                            System.out.println("Rule  " + match.getRule().getName() + " : match failed! " + ex.getMessage());
                        }
                    }
                } else {
                    if (DEBUG) {
                        System.out.println("Rule  " + match.getRule().getName() + " : match is not valid");
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Exception processing some match; skipping it");
            //done[i] = true;
        }

        if (DEBUG) {
//            System.out.println("Rule  " + match.getRule().getName()+ " : match has no more completion");
        }

        gragra.destroyMatch(match);
        //showGraph(graphGrammar);
//        graphGrammar.save(AGGAddress);

        if (DEBUG) {
            System.out.println("After apply rule2  graphGrammar  saved in  " + AGGAddress);
        }

        gratraOptions.add("layered");
        gragra.setGraTraOptions(gratraOptions);

        return gragra;
    }

    public GraGra merge(ArrayList<GraGra> listGraphs) {
        if(DEBUG) System.out.println("currently merging a list of " + listGraphs.size() + "graphs");
        Graph finalGraph = new Graph();
        ArrayList<String> NamesOfNodes = new ArrayList<>();
        for (int i = 0; i < listGraphs.size(); i++) {
            for (int j = 0; j < listGraphs.get(i).getGraph().getNodesList().size(); j++) {
                if (!NamesOfNodes.contains(listGraphs.get(i).getGraph().getNodesList().get(j).getObjectName())) {
                    Node currentNode = listGraphs.get(i).getGraph().getNodesList().get(j);
                    if (currentNode.getObjectName() == "")
                        currentNode.setObjectName("new_component_" + NumberNewComponent++);
                    finalGraph.addNode(currentNode);
                    NamesOfNodes.add(currentNode.getObjectName());
                }
            }
        }

        for (int i = 0; i < finalGraph.getNodesList().size(); i++) {
            Node currentNode = finalGraph.getNodesList().get(i);
//            System.out.println("***CURRENT NODE = "+currentNode.getObjectName());
            //creating Arcs
            List<Arc> outgoings = currentNode.getOutgoingArcsVec();
            List<Arc> ingoings = currentNode.getIncomingArcsVec();
            for (int j = 0; j < finalGraph.getNodesList().size(); j++) {
                //System.out.println("targetOrSourceNode "+finalGraph.getNodesList().get(j).getObjectName());
                Node targetOrSourceNode = finalGraph.getNodesList().get(j);
//                System.out.printf("checking outgoings ");
                for (int k = 0; k < outgoings.size(); k++) {
                    //modify target node
                    Arc a = outgoings.get(k);
//                    System.out.println("CONDITION membre 1 :"+targetOrSourceNode.getObjectName()+" ,membre 2 "+a.getTarget().getObjectName());
                    if (targetOrSourceNode.getObjectName().equals(a.getTarget().getObjectName())) {
                        a.setTarget(targetOrSourceNode);
                        finalGraph.addArc(a);
//                        System.out.println("By checking outgoing Arcs : new Arc Added between "+currentNode.getObjectName()+" and "+targetOrSourceNode.getObjectName());
                    }
                }
//                System.out.printf("checking ingoings ");
                for (int k = 0; k < ingoings.size(); k++) {
                    //modify target node
                    Arc a = ingoings.get(k);
//                    System.out.println("CONDITION membre 1 :"+targetOrSourceNode.getObjectName()+" ,membre 2 "+a.getSource().getObjectName());
                    if (targetOrSourceNode.getObjectName().equals(a.getSource().getObjectName())) {
                        a.setSource(targetOrSourceNode);
                        finalGraph.addArc(a);
//                        System.out.println("By checking ingoing Arcs : new Arc Added between "+currentNode.getObjectName()+" and "+targetOrSourceNode.getObjectName());
                    }
                }
            }
        }

        GraGra finalGragra = graphGrammar;
        finalGragra.importGraph(finalGraph);
        if(DEBUG) System.out.println("Merge is finished");
        return finalGragra;
    }


    public static void showGraph(GraGra gragra) {
        System.out.println("\nGraph: " + gragra.getGraph().getName() + " {");
        Iterator<?> e = gragra.getGraph().getArcsSet().iterator();
        while (e.hasNext()) {
            Arc arc = (Arc) e.next();
            Node src = (Node) arc.getSource();
            Node trg = (Node) arc.getTarget();
            String srcName = src.getObjectName();
            String trgName = trg.getObjectName();

            if (srcName == "")
                srcName = String.valueOf(src);
            if (trgName == "")
                trgName = String.valueOf(trg);


            System.out.println(srcName + " ---"
                    + arc.getType().getStringRepr() + "---> "
                    + trgName);

//            System.out.println(src.getAttribute().getValueAt("id") + " ---"
//                    + arc.getType().getStringRepr() + "---> "
//                    + trg.getAttribute().getValueAt("id"));


        }
        e = gragra.getGraph().getNodesSet().iterator();
        while (e.hasNext()) {
            Node node = (Node) e.next();
            if (node.getIncomingArcsSet().isEmpty()
                    && node.getOutgoingArcsSet().isEmpty())
                System.out.println(node.getAttribute().getValueAt("name"));
        }
        System.out.println(" }\n");
    }

}
