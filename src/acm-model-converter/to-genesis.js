const acm_metamodel = require("acm-metamodel");
const Metamodel = acm_metamodel.Metamodel;

const toNodered = require("./to-nodered.js");
const toThingML = require("./to-thingml.js");

var exports = module.exports = {};
exports.fromModel = fromModel;


function fromModel(model, callback) {
    let genesis = model.models;
	//genesis.url = model.url;

    let toWait = 0;

    for (let i = 0; i < genesis.dm.components.length; i++) {
        // nodered
        if (genesis.dm.components[i].nr_flow && genesis.dm.components[i].nr_flow.length > 0) {
            // acquire list of flows
            let flownodes = genesis.dm.components[i].nr_flow.filter(nrcmpf => nrcmpf.type === "tab");

            if (flownodes.length === 0) {
                console.error("!! toGenesis: nodered flow without tab node, using z as ID");
                flownodes.push({ id : genesis.dm.components[i].nr_flow[0].z });
            }

            flownodes.forEach(flownode => {
                let current_id = flownode.id;
                /*if (genesis.dm.components[i].nr_flow[0].type === "tab") {
                    current_id = genesis.dm.components[i].nr_flow[0].id
                } else {
                    current_id = genesis.dm.components[i].nr_flow[0].z;
                    console.error("! toGenesis: nodered flow without leading tab node, using z as ID");
                }*/
                let subModelComponents = [];
                let subModelLinks = [];

                for (let j = 0; j < model.components.length; j++) {
                    if (current_id === model.components[j].id_parent) {
                        subModelComponents.push(model.components[j]);
                        //add links of current component
                    }
                }
                for (j = 0; j < model.links.length; j++) {
                    //checking only the origin for the moment but can be upgraded to check destination
                    if (model.links[j].from.id_parent === current_id) {
                        subModelLinks.push(model.links[j]);
                    }
                }
                let subMetamodel = new Metamodel(subModelComponents, subModelLinks, genesis.dm.components[i].nr_flow);
                subMetamodel.acm_model_type = "";
                toNodered.fromModel(subMetamodel, function (new_flow, acmgens, mqttneeded) {
                    // genesis probably only eats the retarded node array
                    genesis.dm.components[i].nr_flow = new_flow.nodes;

                    if (acmgens) {
                        // add package ref to install
                        genesis.dm.components[i].packages.push("enact-actuation-conflict-manager-node");
                    }

                    if (mqttneeded) {
                        genesis.dm.components[i].packages.push("node-red-contrib-mqtt-broker");
                    }
                });


                // add mounts
                /*genesis.dm.components[i].docker_resource.mounts = {
                    type: "bind", 
                    src: "/sources/actuation_conflict_manager_nodes",
                    tgt: "/mnt/actuation_conflict_manager_nodes"
                };*/
            });
        }
        // thingml 
        if (genesis.dm.components[i]._type === "/internal/thingml") {
            let current_id = /*genesis.dm.components[i].id + "/" +*/ genesis.dm.components[i].name;
            let subModelComponents = [];
            let subModelLinks = [];

            for (let j = 0; j < model.components.length; j++) {
                if (current_id === model.components[j].id_parent) {
                    subModelComponents.push(model.components[j]);
                    //add links of current component
                }
            }
            for (j = 0; j < model.links.length; j++) {
                //checking only the origin for the moment but can be upgraded to check destination
                if (model.links[j].from.id_parent === current_id) {
                    subModelLinks.push(model.links[j]);
                }
            }
            let subMetamodel = new Metamodel(subModelComponents, subModelLinks, genesis.dm.components[i].nr_flow);
            subMetamodel.acm_model_type = "";
            toWait++;
            toThingML.fromModel(subMetamodel, genesis.dm.components[i], (ret) => { console.log("towait"); toWait-- });
        }
    }

    let activewait = function (){
        if (toWait == 0) {
            callback(genesis);
        } else {
            console.log("activewait settimeout " + toWait);
            setTimeout(() => activewait(), 1000);
        }
    }
    activewait();
    //callback(genesis);
}