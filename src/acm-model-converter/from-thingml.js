var exports = module.exports = {};

const search = require('recursive-search');
const unzipper = require('unzipper');
const fs = require('fs');
const path = require("path");
const crypto = require("crypto");
const child_process = require("child_process");
const http = require("http");

const TMLGO = require("./ThingMLGraphOrientation");
const acm_metamodel = require("acm-metamodel");
const Metamodel = acm_metamodel.Metamodel;
const SoftwareComponent = acm_metamodel.SoftwareComponent;
const Link = acm_metamodel.Link;

//let sourcePath = "B:/Ali/PFE/sos2.zip";
let destinationPath = path.resolve(process.cwd(), "./thingml/x");
let compilationDirectory = path.resolve(process.cwd(), "./thingml/c");

// switch from pure js parsing to java server based parsing
let useJava = true;

exports.fromFilename = function (sourcePath, cmp, callback) {
    if (useJava) {
        callJavaServer({ thingmlInputPath: sourcePath }, (retraw) => {
            let ret = JSON.parse(retraw);
            let res = { components: [], links: [] };

            ret.instances.forEach(instance => {
                res.components.push(new SoftwareComponent(generateNRID(), "[" + instance.name + ":" + instance.type + "]", 0, 0, "SoftwareComponent", cmp.name));
            });

            function cmpbyname(cmp, search) {
                return cmp.name.split(":")[0].substr(1) === search;
            }

            ret.connectors.forEach(conn => {
                res.links.push(new Link(generateNRID(), conn.from + "=>" + conn.to, 0, 0, res.components.find(c => cmpbyname(c, conn.from)), res.components.find(c => cmpbyname(c, conn.to)), { source: conn.fromPort, target: conn.toPort }));
            });

            //console.log(require("util").inspect(res, false, 4, true));

            layout(res.components);
            callback(res);
        });
    } else {
        compilationDirectory = path.resolve(process.cwd(), "./thingml/" + cmp.name + "/");
        let res = first(sourcePath, destinationPath, second);

        compile(res);

        let str = orient(res);
        //console.log(str);

        let model = exports.fromContent(str, cmp.id + "/" + cmp.name);
        callback(model);
    }
}

exports.fromContent = function(fileContent, idparent) {
    let softwarecomponents = [];
    let links = [];

    let lines = String(fileContent).split("\n");
    for (let index = 0; index < lines.length; index++) {
        if (lines[index].length === 0) continue;
        if (lines[index].includes("-->")) {
            let words = lines[index].split("--");
            words = words.map(w => w.replace("\<", "").replace("\>", "").split(" ").join(""));
            let name = words[0].concat("-").concat(words[1]);
            let from;
            let to;
            let comp1 = new SoftwareComponent(generateNRID(), words[0].slice(0), 0, 0, "SoftwareComponent", idparent);
            if (softwarecomponents.findIndex(cmp => cmp.name === comp1.name) === -1) {
                softwarecomponents.push(comp1);
            }
            let comp2 = new SoftwareComponent(generateNRID(), words[1].slice(0), 0, 0, "SoftwareComponent", idparent);
            if (softwarecomponents.findIndex(cmp => cmp.name === comp2.name) === -1) {
                softwarecomponents.push(comp2);
            }
            from = softwarecomponents.find(cmp => cmp.name === comp1.name);
            to = softwarecomponents.find(cmp => cmp.name === comp2.name);
            let portSplit = words[2].split("/");
            links.push(new Link(generateNRID(), name, 0, 0, from, to, /*0, false,*/ { source: portSplit[0], target: portSplit[1] }));
        }
        if (lines[index].includes("<--")) {
            let words = lines[index].split("--");
            words = words.map(w => w.replace("\<", "").replace("\>", "").split(" ").join(""));
            let name = words[1].concat("-").concat(words[0]);
            let from;
            let to;
            let comp1 = new SoftwareComponent(generateNRID(), words[0].slice(0), 0, 0, "SoftwareComponent", idparent);
            if (softwarecomponents.findIndex(cmp => cmp.name === comp1.name) === -1) {
                softwarecomponents.push(comp1);
            }
            let comp2 = new SoftwareComponent(generateNRID(), words[1].slice(0), 0, 0, "SoftwareComponent", idparent);
            if (softwarecomponents.findIndex(cmp => cmp.name === comp2.name) === -1) {
                softwarecomponents.push(comp2);
            }
            from = softwarecomponents.find(cmp => cmp.name === comp2.name);
            to = softwarecomponents.find(cmp => cmp.name === comp1.name);
            let portSplit = words[2].split("/");
            links.push(new Link(generateNRID(), name, 0, 0, from, to, /*0, false,*/ { source: portSplit[1], target: portSplit[0] }));
        }
    }
    layout(softwarecomponents);
    //console.log(softwarecomponents);
    //console.log(links);
    model = new Metamodel(softwarecomponents, links);
    return model;
}

function layout(components) {
    // sqrt(mbflow) wide, go up one y axis step every time you reach it
    let gridcols = Math.ceil(Math.sqrt(components.length));
    //console.log("ACM layout " + gridcols);
    let xbase = 0, ybase = 0, xmax = 0, ymax = 0;
    for (let cmpidx in components) {
        let cmp = components[cmpidx]
        cmp.x += xbase;
        cmp.y += ybase;

        if (cmp.x > xmax) { xmax = cmp.x; }
        if (cmp.y > ymax) { ymax = cmp.y; }

        if ((parseInt(cmpidx) + 1) % gridcols === 0) {
            //console.log("going to next row");
            // next row
            xbase = 0;
            xmax = 0;
            ybase = ymax + 100;
        } else {
            // next col
            xbase = xmax + 100;
        }
    }
}

function generateNRID() {
    return generateID(8) + "." + generateID(6);
}

// generate hex id https://stackoverflow.com/a/27747377
// dec2hex :: Integer -> String
function dec2hex(dec) {
    return ('0' + dec.toString(16)).substr(-2);
}

// generateId :: Integer -> String
function generateID(len) {
    var arr = new Uint8Array((len || 40) / 2);
    crypto.randomFillSync(arr);
    return Array.from(arr, dec2hex).join('');
}

function first(sourcePath, destinationPath, callback) {
    if (sourcePath.endsWith(".zip")) {
        fs.createReadStream(sourcePath)//Path à récupérer depuis le modèle GeneSIS
            .pipe(unzipper.Extract({ path: destinationPath })); // Path d'extraction à définir de façon relative
    } else {
        return sourcePath;
    }
    return callback(destinationPath);
}

function second(destinationPath) {
    return search.recursiveSearchSync(/.thingml$/, destinationPath)[0];
    //return search.recursiveSearchSync(/_merged.thingml/, compilationDirectory)[0];
}

function compile(res) {
    let child = child_process.execSync("java -jar ./thingml/ThingML2CLI.jar -o \"" + compilationDirectory + "\" -c uml -s \"" + res + "\"");
    console.log(child.toString());
}

function orient(res) {
    //let plantumlfilename = search.recursiveSearchSync(/.plantuml$/, compilationDirectory)[0];
    let mergedthingmlfilename = search.recursiveSearchSync(/_merged.thingml/, compilationDirectory)[0];
    let plantumlfilename = path.dirname(mergedthingmlfilename, "_merged.thingml") + path.sep + path.basename(mergedthingmlfilename, "_merged.thingml") + ".plantuml";
    TMLGO.reset();
    let str = TMLGO.readSingleFile2(res, plantumlfilename);
    return str;
}

function callJavaServer(payload, callback) {
    let options = {
        hostname: 'localhost',
        port: 8007,
        path: '/fromThingML',
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        }
    };
    let req = http.request(options, function (res) {
        // get data as its streamed by the server
        let buff = "";
        res.on("data", function (chunk) {
            buff += chunk;
        });

        // request finish start processing back
        res.on("end", function () {
            callback(buff);
        });
    });
    req.write(JSON.stringify(payload));
    req.end();
}

//let mergedthingmlfilename = search.recursiveSearchSync(/_merged.thingml/, 'D:\\Documents\\I3S\\_vsprojects\\acm-app\\thingml\\MainCfg')[0];