import React, { Component } from 'react';
import './App.css';
import "antd/dist/antd.css";
import ACMAppRoot from './components/ACMAppRoot';

class App extends Component {
	render() {
		return (
			<ACMAppRoot/>
		);
	}
}

export default App;
