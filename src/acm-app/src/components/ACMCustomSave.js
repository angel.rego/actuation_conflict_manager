import React, { Component } from 'react';
import { Modal, Button, Form, Input } from 'antd';

export default class ACMCustomSave extends Component {
	constructor(props) {
		super(props);

		this.state = {
			parent: this.props.parent
		};

		this.onNameChange = this.onNameChange.bind(this);
		this.onDescriptionChange = this.onDescriptionChange.bind(this);

	}

	onNameChange(e) {
		this.state.parent.setState({ ACMCustomName: e.target.value });
	}

	onDescriptionChange(e) {
		this.state.parent.setState({ ACMCustomDescription: e.target.value });
	}

	render() {
		return (
			<Modal
				title="information for new ACM registration"
				footer={[
					<Button key="ACMCustomSaveCancel" onClick={this.state.parent.closesaveACMCustomModal}>Cancel</Button>,
					<Button key="ACMCustomSaveSelect" onClick={this.state.parent.saveACMCustom}>Save</Button>
				]}
				visible={this.state.parent.state.saveACMCustomModalOpen}
				onCancel={this.state.parent.closesaveACMCustomModal}
			>
				<Form>
					<Form.Item label="Name">
						<Input onChange={this.onNameChange} value={this.state.parent.state.ACMCustomName} autoFocus />
					</Form.Item>
					<Form.Item label="Description">
						<Input onChange={this.onDescriptionChange} value={this.state.parent.state.ACMCustomDescription} autoFocus />
					</Form.Item>
					
				</Form>
			</Modal>
		);
	}
} 