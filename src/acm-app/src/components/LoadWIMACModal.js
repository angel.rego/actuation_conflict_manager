﻿import React, { Component } from 'react';
import { Modal, Input, Button, Upload, Icon, Alert } from 'antd';
const { Dragger } = Upload;
const InputGroup = Input.Group;

export default class LoadWIMACModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            parent: this.props.parent,
            disableUpload: false,
            fileList: [],
            alertVisible: false
        };

        this.loadWIMAC = this.loadWIMAC.bind(this);
        this.closeLoadWIMACModal = this.closeLoadWIMACModal.bind(this);
    }

    componentDidMount() {

    }

    loadWIMAC() {
        // check file existence
        if (this.state.fileList.length === 0) {
            this.setState({ alertFileVisible: true });
            return;
        } 

        // actually load
        this.state.parent.loadWIMACFromFile(this.state.fileList[0]);

        // clear data
        this.setState({ fileList: [], disableUpload: false });
    }

    closeLoadWIMACModal() {
        this.setState({ fileList: [], disableUpload: false });
        this.state.parent.closeLoadWIMACModal();
    }

    render() {
        const props = {
            name: 'file',
            multiple: false,
            beforeUpload: (file) => {
                this.setState({ disableUpload: true });
                this.state.fileList.push(file);
                // return false to not actually upload from the Dragger, instead we upload manually on select button
                return false;
            },
            onRemove: (f) => {
                this.setState({ fileList: this.state.fileList.filter(fi => fi===f), disableUpload: false });
                return true;
            }
        };

        return (
            <Modal
                title="Select WIMAC file"
                footer={[
                    <Button key="loadwimaccancel" onClick={this.closeLoadWIMACModal}>Cancel</Button>,
                    <Button id="loadwimacselect" key="loadwimacselect" onClick={this.loadWIMAC}>Load</Button>
                ]}
                visible={this.state.parent.state.loadWIMACModalOpen}
                onCancel={this.state.parent.closeLoadWIMACModal}
            >
                <InputGroup compact>
                    <div id="sel-file-dragger">
                        <Dragger {...props} disabled={this.state.disableUpload} fileList={this.state.fileList}>
                            <p className="ant-upload-drag-icon">
                                <Icon type="inbox" />
                            </p>
                            <p className="ant-upload-text">Click to open dialog, or drag WIMAC model to this area to load it.</p>
                        </Dragger>
                    </div>
                </InputGroup>
                {this.state.alertFileVisible ? (<div className="alert"><Alert message="Select a file" type="error" /></div>) : null}
            </Modal>
        );
    }
}