var ACMElement = require("./ACMElement.js");

class PhysicalProcess extends ACMElement {
    constructor(id, name, x, y, location, effect) {
        super(id, name, x, y);
        this.location = location;
        this.effect = effect;
    }
}

module.exports = PhysicalProcess;