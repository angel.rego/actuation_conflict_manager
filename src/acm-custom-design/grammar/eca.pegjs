
Main = synchro:SynchroConfiguration _ predicateConfiguration:PredicateConfiguration* _ fsm:stateMachine _ actionDefinition:ActionDefinition* _ DesynchroConfiguration* _ properties:Properties*_ environmentConfiguration:EnvironmentConfiguration* _{
    //return {"actionDefinition":fsm}
    return {
            "synchro":synchro,
            "predicateConfiguration":predicateConfiguration,
            "fsm":fsm,
            "actionDefinition":actionDefinition,
            "properties":properties,
            "environmentConfiguration":environmentConfiguration
    }
}

SynchroConfiguration = "IF" _ condition:SynchroCondition? _ delay:SynchroDelay? _ "TRIGGER"{
    return {"condition":condition,"delay": delay}
} 

SynchroCondition = cond:predicateCondition _ "THEN"{
    return cond
}

SynchroDelay = "DELAY" _ delay:Integer _ "ms"_ "THEN"{
    return delay
}

PredicateConfiguration = _ predicate:predicate _ "=" _ condition:predicateCondition _{
    return {"predicate":predicate,"condition":condition}
}

predicateCondition = left:InputCondition _ '||' _ right:predicateCondition {return {"left":left,"operation":"||","right":right}}
            / left:InputCondition _ '&&' _ right:predicateCondition {return {"left":left,"operation":"&&","right":right}}
            // '(' condition:predicateCondition ')' {return {"expr":condition,"operation":"()"}}
            / condition:InputCondition {return condition}
            

InputCondition = condition:(IntegerInput/ DoubleInput/ StringInput){
    return condition        
}
/ '(' condition:predicateCondition ')' {return {"expr":condition,"operation":"()"}}
        
IntegerInput = input:Input _ element:("<="/">="/"=="/"!="/"<"/">") _ int:Integer _  {
    return {"input":input,"operation":element,"value":int}
    //return {"condition": "input_"+input.inputPort+"_"+input.index + " " + element+" "+ int}
} 

DoubleInput = input:Input _ element:("=="/"!="/"<"/">"/"<="/">=") _ double:Double _{
    return {"input":input,"operation":element,"value":double}
    //return {"condition": "input_"+input.inputPort+"_"+input.index +element+double}
}
        
StringInput = input:Input _ element:("=="/"!=") _ word:Word _ {
    return {"input":input,"operation":element,"value":word}
    //return {"condition": "input_"+input.inputPort+"_"+input.index +element+word}
}

stateMachine = inits:InitFSM* _ eca:ECARules* _ defaultMode:DefaultRule _{
    return {"eca":eca,"inits":inits,"default":defaultMode}
}

InitFSM = init:VariableAssignement _{
    return init
}
//
ECARules = _ "ON" _ event:predicate _ "IF" _ cond:VariableCondition _ "DO" _ transition:VariableAssignement* _ action:predicate* _ ";" _{
    var transition = {"event":event,"condition":cond,"transition":transition,"actions":action}
    return transition
}

DefaultRule = "DEFAULT" _ "DO" _ action:predicate* _ ";"{
    var transition = {"event":"default","actions":action}
    return transition
}

VariableAssignement = variable:variable _ "=" _ value:Word _{
    return {"variable":variable,"value":value}
}

VariableCondition = left:VariableConditionMember _ '||' _ right:VariableCondition {return {"left":left,"operation":"||","right":right}}
            / left:VariableConditionMember _ '&&' _ right:VariableCondition {return {"left":left,"operation":"&&","right":right}}
            // '(' condition:VariableCondition ')' {return {"expr":condition,"operation":"()"}}
            / condition:VariableConditionMember {return condition}

VariableConditionMember =  variable:variable _ element:("=="/"!=") _ value:Word {return {"variable":variable,"operator":element,"value":value}}
                    / _"True" _ {return "true"}
                    / '(' condition:VariableCondition ')' {return {"expr":condition,"operation":"()"}}


ActionDefinition = _ actionName:predicate _ ":" first:OutputAssignement _ next:ActionDefinitionSuite*{
        var assignement = [first];
        for (var i = 0; i< next.length ;i++){
            assignement.push(next[i])
        }
        return {"actionName":actionName,"assignement":assignement}
}
ActionDefinitionSuite = _ "," item:OutputAssignement _  {
    return item
}

OutputAssignement = _ output:Output _ "=" _ value:(ActionFunction/ActionOperation/ Input){
    return {"output":output,"value":value}
}

ActionOperation = _ left:Input _ operation:("+"/"-"/"*"/"/") _ right:ActionOperation _ {return {"operation":operation,"args":[left,right]}}
                    / _ input:Input{return input}
                    / _ constant:(Integer/Word/Double){return constant}

ActionFunction = f:predicate _ "(" _ first:Input _ next:ActionSuite* _ ")"_{
    var args = [first]
    for(var i = 0; i<next.length; i++){
        args.push(next[i])
    }
    return {"function":f,"args":args}
}
ActionSuite = "," _ input:Input  _{
        return input    
}

Output =  "Output("_ outputPort:Integer _ "," _ index:Integer _ ")"{
    return  {"outputPort":outputPort,"index":index}
}


Input =  "Input("_ inputPort:Integer _ "," _ index:Integer _ ")"{
    return  {"inputPort":inputPort,"index":index}
}

DesynchroConfiguration = "toto"

Properties = "ASSERT" _ "!(" _ condition:PropertyCondition _")"_ "within" _ time:Integer _ "ms" _{
       var property = {}
       return {"time":time,"property":condition}
}

PropertyCondition = left:PropertyConditionMember _ '||' _ right:PropertyCondition {return {"left":left,"operation":"||","right":right}}
            / left:PropertyConditionMember _ '&&' _ right:PropertyCondition {return {"left":left,"operation":"&&","right":right}}
            / '(' condition:PropertyCondition ')' {return {"expr":condition,"operation":"()"}}
            / condition:PropertyConditionMember {return condition}

PropertyConditionMember= left:(Input/Output) _ element:("=="/"!="/"<"/">"/"<="/">=") _ right:(Integer/Word) _{
    if(element=="=="){
        element = "="
    }
    return  {"operation":element,"left":left,"right":right,"args":[left,right]}
}
/ '(' condition:PropertyCondition ')' {return {"expr":condition,"operation":"()"}}
    

EnvironmentConfiguration = input:Input _ "=" _ config:(EnvironmentIntConfiguration/EnvironmentStringConfiguration) _ {
    if(config.min!=undefined){
        return {"inputPort":input.inputPort,"index":input.index, "min":config.min,"max":config.max}
    }else{
        return {"inputPort":input.inputPort,"index":input.index, "setOfInput":config}
    }
}

EnvironmentIntConfiguration =  "[" _ min:Integer _ "," _ max:Integer _ "]" _{
    return {"min":min,"max":max}
}

EnvironmentStringConfiguration =  "{" _ strings:Word* _ "}" _{
    return strings
}

////////////////////////////////////////////////////////////////////
Word = '\"' l:Letter+ '\"' _
 { return l.join(""); }

NULL = 'null'{
    return "null"
}

predicate = l:Letter+ _
 { return l.join(""); } 

variable = l:Letter+
 { return l.join(""); } 

Digit = [0-9]

Letter
 = [a-zA-Z_]

Double = [0-9]+.[0-9]+ / NULL

Integer "integer" 
  = _ [0-9]+ { return parseInt(text(), 10); } / NULL
  
ws "Whitespace"
  = [ \t\n\r]

_ "One or more whitespaces"
 = ws*

/*
IF Input(0,0)!=null && Input(1,0)!=null THEN DELAY 1000 ms THEN TRIGGER

noConflict = (Input(0,0) == "alarm_off" && Input(1,0) == "start_music") || (Input(0,0) == "alarm_on" && Input(1,0) == "stop_music") || (Input(0,0) == "alarm_off" && Input(1,0) == "stop_music")
conflict = Input(0,0) == "alarm_on" && Input(1,0) == "start_music"

ON conflict IF (hello == "cucou" && hello == "cucou") || hello == "cucou"  DO doAlarm  ;
ON noConflict IF True DO letThrough;
DEFAULT DO letThrough;

doAlarm : Output(0,0) = "alarm_on", Output(1,0) = "stop_music"
letThrough : Output(0,0) = Input(0,0), Output(1,0) = Input(1,0) 

ASSERT!(Output(0,0) == "alarm_on" && Output(1,0) == "start_music") within 100 ms

Input(0,0) = {"alarm_on" "alarm_off"}
Input(1,0) = {"start_music" "stop_music"}



*/

