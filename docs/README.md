# ACM documentation index

## Metamodel
- [Class diagram](./ACM%20metamodel.jpg)
- [ecore](./ACM.ecore)


## Examples
- [Node-RED direct conflict resolution](./tutorials/1.node-red/README.md)
  - Use ACM to solve a direct conflict in a Node-RED flow
- [Node-RED indirect conflict resolution](./tutorials/2.node-red-env/README.md)
  - Use ACM to solve an indirect conflict in a Node-RED flow, using an environment model
- [Node-RED indirect conflict resolution](./tutorials/3.genesis/README.md)
  - Use ACM to solve an indirect conflict in two separate Node-RED flows, using GeneSIS to deploy the application