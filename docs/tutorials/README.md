# ACM Tutorials index

- [Node-RED direct conflict resolution](./1.node-red/README.md)
  - Use ACM to solve a direct conflict in a Node-RED flow
- [Node-RED indirect conflict resolution](./2.node-red-env/README.md)
  - Use ACM to solve an indirect conflict in a Node-RED flow, using an environment model
- [Node-RED indirect conflict resolution](./3.genesis/README.md)
  - Use ACM to solve an indirect conflict in two separate Node-RED flows, using GeneSIS to deploy the application